import { Component, OnInit } from '@angular/core';
import { QuoteService } from './quote.service';
import { Http, Response } from '@angular/http';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('flipState', [
      state('true', style({
        transform: 'rotateY(179.9deg)'
      })),
      state('false', style({
        transform: 'rotateY(0)'
      })),
      transition('true => false', animate('500ms ease-out')),
      transition('false => true', animate('500ms ease-in'))
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'Valeria,';
  data: Array<any>;
  flip: string = 'inactive';
  items: Array<any> = [];
  giphies: any;
  inputText: string;

  constructor(private quoteService: QuoteService,
    private http: Http) {
    this.items = [
      { name: 'assets/images/IMAG0242.jpg' },
      { name: 'assets/images/IMAG0674.jpg' },
      { name: 'assets/images/IMG-20170707-WA0001.jpg' },
      { name: 'assets/images/IMAG0617.jpg' }
    ]
  }

  ngOnInit() {
    this.data = this.quoteService.getData();
  }

  toggleFlip(note) {
    note.toggled = (note.toggled == false) ? true : false;
  }

  getGiphy() {
    console.log(this.inputText);
    this.http.request("http://api.giphy.com/v1/gifs/random?api_key=f9e5438b4f5c48efb6b0e87fec125e2d&tag="+this.inputText).
      subscribe((res: Response) => {
        this.giphies = res.json().data; console.log(this.giphies);
      }
      );
  }

}
