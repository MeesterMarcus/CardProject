import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import { DATA } from './data';

@Injectable()
export class QuoteService {

  constructor(private http: Http) { }

  getData() {
    return DATA;
  }

}
