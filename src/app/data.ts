
export const DATA: any[] = [
  {
        'id': 1,
        'quote': 'You\'re the most amazing girl I\'ve ever met.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 2,
        'quote': 'I\'m so in love with your smile.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 3,
        'quote': 'I\'m so lucky to have met you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 4,
        'quote': 'I can\'t stop thinking about.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 5,
        'quote': 'I love falling asleep with you in my arms.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 6,
        'quote': 'I feel most alive when I\'m with you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 7,
        'quote': 'Thank you for being patient with me and understanding.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 8,
        'quote': 'Never stop being you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 9,
        'quote': 'You\'re an amazing cook.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 10,
        'quote': 'I can be myself when I\'m with you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 11,
        'quote': 'I still feel that invisible string everytime we part ways.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 12,
        'quote': 'I love waking up with you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 13,
        'quote': 'I dread the words "Can you drop me off?".',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 14,
        'quote': 'Thank you for choosing me.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 15,
        'quote': 'You\'re an evil genius.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 16,
        'quote': 'Te amo.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 17,
        'quote': 'You mean the world to me.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 18,
        'quote': 'I still get butterflies when I\'m with you.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 19,
        'quote': 'I love it when you attack me with kisses.',
        'img': 'some img src',
        'toggled': false
    },
    {
        'id': 20,
        'quote': 'I want to spend my life with you.',
        'img': 'some img src',
        'toggled': false
    }
];