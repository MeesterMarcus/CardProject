import { ValPage } from './app.po';

describe('val App', () => {
  let page: ValPage;

  beforeEach(() => {
    page = new ValPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
